# Octabio Headings

To use the Octabio board on Arduino IDE you need to use this headings to identify the correct pin number for every port

    PIN_A
    PIN_B
    PIN_E
    PIN_F
    PIN_BUZZER
    PIN_BATTERY
