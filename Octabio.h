 #ifndef OCTABIO
#define OCTABIO

#include "Arduino.h"

// Start octavio config //
#define PIN_A A2
#define PIN_E 4
#define PIN_F 3
#define PIN_B A1
#define PIN_BATTERY A0
#define PIN_BUZZER 2
// End octavio config //

#endif